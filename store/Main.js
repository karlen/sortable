import { createStore } from 'redux'
import Banner_1240_100 from 'modules/Banner_1240_100'
import HeaderContent from 'modules/HeaderContent'
import Shares from 'modules/Shares'
import BestOfCarousel from 'modules/BestOfCarousel'
import Desktinations from 'modules/Desktinations'
import LaunchbreackAdventures from 'modules/LaunchbreackAdventures'
import EventWrapUp from 'modules/EventWrapUp'
import BannerFooter from 'modules/BannerFooter'
import Filters from 'modules/Filters'
import {arrayMove} from 'react-sortable-hoc';


class Store{

    constructor(){
        this.postId = parseInt( document.getElementById('app').getAttribute('data-id') ) || 0
        this.allModules = [
            {
                module: <Banner_1240_100  />,
                moduleName: "banner_1240_100",
                "img": "assets/images/banner_top.gif",
                value: ""
            },
            {
                module: <HeaderContent  />,
                moduleName: "HeaderContent",
                "img": "assets/images/header_content.png",
                value: ""
            },
            {
                module: <Shares  />,
                moduleName: "Shares",
                "img": "assets/images/shares.png",
                value: ""
            },
            {
                module: <Filters  />,
                moduleName: "Filters",
                "img": "assets/images/filters.png",
                value: ""
            },
            {
                module: <BestOfCarousel  />,
                moduleName: "BestOfCarousel",
                "img": "assets/images/bestof.png",
                value: ""
            },
            {
                module: <Desktinations  />,
                moduleName: "Desktinations",
                "img": "assets/images/desktionations.png",
                value: ""
            },
            {
                module: <LaunchbreackAdventures  />,
                moduleName: "LaunchbreakAdventures",
                "img": "assets/images/launchbreack.png",
                value: ""
            },
            {
                module: <EventWrapUp data={{moduleName: "EventWrapUp"}} />,
                moduleName: "EventWrapUp",
                "img": "assets/images/eventwrapup.png",
                value: ""
            },
            {
                module: <BannerFooter data={{moduleName: "BannerFooter"}} />,
                moduleName: "BannerFooter",
                "img": "assets/images/banner_top.gif",
                value: ""
            }
        ]
        this.store = createStore(this.doAction.bind(this), {
                state: {
                    postId: this.postId,
                    items: this.allModules,
                    mobileModules: [],
                    tabletModules: [],
                    desktopModules: [],
                    allModules: this.allModules,
                    postData: {},
                    showRemove: false,
                    hoveredRemove: false,
                    hoveredCopy: false,
                    activeNode: false,
                    views: ["desktop", "tablet", "mobile"],
                    view: 'desktop',
                    showSideBar: false
                },
                cb: null
            }
        )
        this.action = null
    }

    hoveredRemove(state){
        state.hoveredRemove = !state.hoveredRemove
        return {
            state: state,
            cb: this.cb
        }
    }

    hoveredCopy(state){
        state.hoveredCopy = !state.hoveredCopy
        return {
            state: state,
            cb: this.cb
        }
    }

    handlerModulesList(state){
        state.showSideBar = !state.showSideBar
        return {
            state: state,
            cb: this.cb
        }
    }

    onSortStart(state){
        state.showRemove = true
        state.hoveredRemove = false
        state.hoveredCopy = false
        state.activeNode =  {
            node: this.action.node,
            index: this.action.index
        }
        return {
            state: state,
            cb: this.cb
        }
    }

    removeItem(state){
        var items = []
        state.items.map(function(el, idx){
            if(idx !== state.activeNode.index){
                items.push(el)
            }
        })
        state.items = items
        state.showRemove = false
        state.hoveredRemove = false
        state.activeNode= false
        return {
            state: state,
            cb: this.cb
        }
    }

    addItem(state){
        var moduleName = this.action.moduleName
        var addItem = []
        state.allModules.map(function(el, idx){
            if(el.moduleName == moduleName){
                addItem.push(el)
            }
        })
        state.items = addItem.concat( state.items )
        return {
            state: state,
            cb: this.cb
        }
    }

    copyItem(state){
        var items = []
        state.items.map(function(el, idx){
            if(idx == state.activeNode.index){
                items.push(el)
                items.push(el)
            }else {
                items.push(el)
            }
        })
        state.items = items
        state.showRemove = false
        state.hoveredCopy = false
        state.activeNode= false
        return {
            state: state,
            cb: this.cb
        }
    }

    changeView(state){

        var count = state.views.length - 1
        var cur = state.views.indexOf(state.view)
        var next = (cur + 1) > count ? 0 : cur+1
        state.view = state.views[next]
        state.items = (state.views[next] == 'mobile') ? state.mobileModules : (state.views[next] == 'tablet') ? state.tabletModules : state.desktopModules

        return {
            state: state,
            cb: this.cb
        }
    }

    onSortEnd(state){

        state.items = arrayMove(state.items, this.action.oldIndex, this.action.newIndex)
        state.showRemove  =  false
        state.hoveredRemove  =  false
        state.hoveredCopy  =  false
        state.activeNode  =  false

        return {
            state: state,
            cb: this.cb
        }
    }




    doAction(state, action){
        this.action = action
        state = state.state
        this.cb = action.callback || null
        var func = this[action.type]
        if (typeof func == 'function') {
            return this[action.type](state);
        } else {
            return {
                state: state,
                cb: this.cb
            }
        }
    }
}





const store = new Store()

export  default store.store