'use strict';

var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var components_folder = path.resolve('./')+"/components";
var modules_folder = path.resolve('./')+"/modules";

function Plugin(){

  function generateMainScss(callback){
    var scss_components = getDirectories(components_folder, 'scss');
    var scss_modules = getDirectories(modules_folder, 'scss');
    var main_scss = path.resolve('./')+"/assets/scss/main.scss";
    copy_to_tmp(main_scss, function(){
      var imports = "";
      scss_components.map(function(item, index) {
        imports += "@import '../../components/"+item+"/index';\r\n";
      });
      scss_modules.map(function(item, index) {
        imports += "@import '../../modules/"+item+"/index';\r\n";
      });
      fs.appendFile(main_scss, imports, function (err) {
        console.log('added '+(scss_modules.length + scss_components.length)+' scss modules');
        console.log("\r\n************************************************\r\n");
        console.log(scss_modules.concat(scss_components));
        console.log("\r\n************************************************\r\n");
        callback();
      });
    });


  }

  function getDirectories(srcpath, type) {
    return fs.readdirSync(srcpath).filter(function(file) {
      return (fs.statSync(path.join(srcpath, file)).isDirectory() && fileExists(path.join(srcpath, file)+'/index.'+type));
    });
  }

  function copy_to_tmp(file, callback){
    var rs = fs.createReadStream(file);
    var ws = fs.createWriteStream(file+'.tmp');
    rs.pipe(ws);
    ws.on( 'close', callback);
  }

  function fileExists(filePath){
    try
    {
      return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
      return false;
    }
  }

  this.apply = function(compiler) {

    compiler.plugin("make",function(compilation, webpack_make_callback){
      generateMainScss(function(){
        webpack_make_callback();
      });
    });

    compiler.plugin("done",function(compilation, webpack_done_callback){
      var main_scss = path.resolve('./')+"/assets/scss/main.scss";

      fs.unlink(main_scss, function(){

      });

      fs.rename(main_scss+'.tmp', main_scss);
    });
  }

}

module.exports = Plugin;
