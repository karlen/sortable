import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import Remove from 'modules/Remove'
import Copy from 'modules/Copy'
import Navigation from 'modules/Navigation'
import Sidebar from 'modules/Sidebar'
import store from  'store/Main'

const SortableItem = SortableElement(({value}) => <li data-module-name={value.moduleName}>{value.module}</li>)

const SortableList = SortableContainer(({items}) => {
    return (
        <ul id="main">
            {items.map((value, index) =>
                <SortableItem key={`item-${index}`} index={index} value={value} />
            )}
        </ul>
    )
})

class Main extends React.Component {

    constructor(props){
        super(props)
        this.state = store.getState().state
    }

    componentDidMount() {
        store.subscribe(() => {
                var st = store.getState()
                this.setState(st.state, st.cb)
            }
        )
    }

    updateMQ() {
        var _self = this
        var allDomEls = document.body.getElementsByTagName('*')
        var i
        for (i = 0; i <	allDomEls.length; i++) {
            var el = allDomEls[i]
            el.classList.remove("mobile", "desktop", "tablet")
            el.classList.add(_self.state.view)
        }
    }

    animate(idx = false, animation = false) {
        if(idx === false || animation == false) return
        idx = parseInt(idx)
        var nextNode = document.getElementById('main').childNodes.item(idx)
        nextNode.classList.add(animation)
        setTimeout(() => {
            nextNode.classList.remove(animation)
        }, 300)
    }

    onSortMove(e) {
        var _self = this
        var w = window.innerWidth
        var h = window.innerHeight
        var remove_area = {
            x1: 0,
            y1: 0+window.scrollY,
            x2: 25+90,
            y2: h+window.scrollY
        }


        var copy_area = {
            x1: w-90-25,
            y1: 0+window.scrollY,
            x2: w,
            y2: h+window.scrollY
        }

        if(_self.state.showRemove == true){

            e = e || window.event
            if (
                (e.pageX >= remove_area.x1 && e.pageX <= remove_area.x2 )  &&
                (e.pageY >= remove_area.y1 && e.pageY <= remove_area.y2)
            ) {
                if(_self.state.hoveredRemove == false){
                    store.dispatch({ type: 'hoveredRemove' })
                }
            }else {
                if(_self.state.hoveredRemove == true){
                    store.dispatch({ type: 'hoveredRemove' })
                }
            }


            if (
                (e.pageX >= copy_area.x1 && e.pageX <= copy_area.x2 )  &&
                (e.pageY >= copy_area.y1 && e.pageY <= copy_area.y2)
            ) {
                if(_self.state.hoveredCopy == false){
                    store.dispatch({ type: 'hoveredCopy' })
                }
            }else {
                if(_self.state.hoveredCopy == true){
                    store.dispatch({ type: 'hoveredCopy' })
                }
            }

        }
    }

    addClickHandler(){
        (this.state.showSideBar == true) ? this.closeModulesList(): this.showModulesList()
    }

    showModulesList(){
        store.dispatch({ type: 'handlerModulesList' })
        document.getElementById("add").classList.add("opened")
    }

    closeModulesList(){
        store.dispatch({ type: 'handlerModulesList' })
        document.getElementById("add").classList.remove("opened")
    }

    onSortStart({node, index, collection}){
        document.body.style.cursor="move"
        store.dispatch({ type: 'onSortStart', node: node, index: index })
    }

    removeItem() {
        store.dispatch({
            type: 'removeItem',
            callback:this.updateMQ
        })
    }

    addItem(moduleName) {
        var _self = this
        store.dispatch({
            type: 'addItem',
            moduleName:moduleName,
            callback: function(){
                _self.animate(0, 'pulse')
                _self.updateMQ()
            }
        })
    }

    copyItem() {
        var _self = this
        var next = _self.state.activeNode.index+1

        store.dispatch({
            type: 'copyItem',
            callback: function(){
                _self.animate(next, 'pulse')
                _self.updateMQ()
            }
        })
    }

    changeView(){
        var _self = this
        var count = this.state.views.length - 1
        var cur = this.state.views.indexOf(this.state.view)
        var next = (cur + 1) > count ? 0 : cur+1

        store.dispatch({
            type: 'changeView',
            callback: function(){
                var allDomEls = document.body.getElementsByTagName('*')
                var i
                for (i = 0; i <	allDomEls.length; i++) {
                    var el = allDomEls[i]
                    el.classList.remove("mobile", "desktop", "tablet")
                    el.classList.add(_self.state.views[next])
                }
            }
        })
    }

    isRemove(){
        return (this.state.hoveredRemove == true && this.state.activeNode) ? true : false
    }

    isCopy(){
        return (this.state.hoveredCopy == true && this.state.activeNode) ? true : false
    }

    onSortEnd({oldIndex, newIndex}){
        document.body.style.cursor=""
        if(this.isRemove()){
            this.removeItem()
            return 0
        }
        if(this.isCopy()){
            this.copyItem()
            return 0
        }
        store.dispatch({
            type: 'onSortEnd',
            oldIndex: oldIndex,
            newIndex: newIndex,
            callback: this.updateMQ
        })
    }

    render() {
        return (
            <div>
                <SortableList
                    items={this.state.items}
                    pressDelay={0}
                    onSortMove={this.onSortMove.bind(this)}
                    useWindowAsScrollContainer={true}
                    onSortStart={this.onSortStart.bind(this)}
                    onSortEnd={this.onSortEnd.bind(this)}
                />

                <Remove />

                <Copy />

                <Navigation
                    onAdd={this.addClickHandler.bind(this)}
                    onViewChange={this.changeView.bind(this)}
                />

                <Sidebar
                    onAdd={this.addItem.bind(this)}
                />
            </div>
        )
    }
}

export default Main