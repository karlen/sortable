
class HeaderContent extends React.Component {

    constructor(props){
        super(props)
        this.state = {

        }
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <div data-module="header_content">
                <div className="canvas cover">
                   <img  src="http://concreteplayground.com/content/uploads/2016/02/Northern-Lights.jpg" alt="New Melbourne Openings to Put in Your Diary" />
                </div>
                <div className="content-inner">
                    <h1 className="title"><a href="#">NEW MELBOURNE OPENINGS TO PUT IN YOUR DIARY</a></h1>
                    <p>
                        Despite the cold weather making&nbsp;a cup of Milo in bed seem just as (if not more) appealing than a three-course meal with matched wine, Melbourne's still opening plenty of&nbsp;new restaurants, cafes and bars — and getting people into them, too. Seems every week we're ranting and raving about the next newbie, bringing its own proposed offering/theming/novelty viral food item to this fine city of ours.
                    </p>
                </div>
                <div className="clear"></div>
            </div>
        )
    }
}

export default HeaderContent