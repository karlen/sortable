
class Shares extends React.Component {

    constructor(props){
        super(props)
        this.state = {

        }
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <div data-module="shares">
                <div className="cp_share_widget">
                    <div className="cp_share_widget_outer">
                        <div className="cp_share_widget_inner">
                            <span className="counter">
                                <span className="counter_outer">
                                    <span className="counter_inner">
                                        <span className="counter_value">125</span>
                                        <span className="counter_label">shares</span>
                                    </span>
                                </span>
                            </span>
                            <div className="share_buttons">

                                <div className="cp-share-button cp-share-button-facebook">
                                    <span className="cp-share-button-outer">
                                        <span className="cp-share-button-inner">
                                            <i className="sprite social social-icons facebook white"></i>
                                            <span className="cp-share-button-text">Facebook</span>
                                        </span>
                                    </span>
                                </div>

                                <div className="cp-share-button cp-share-button-twitter">
                                   <span className="cp-share-button-outer">
                                       <span className="cp-share-button-inner">
                                           <i className="sprite social social-icons twitter white"></i>
                                           <span className="cp-share-button-text">Twitter</span>
                                       </span>
                                   </span>
                                </div>

                                <div  className="cp-share-button cp-share-button-email">
                                   <span className="cp-share-button-outer">
                                       <span className="cp-share-button-inner">
                                           <i className="sprite social social-icons email white"></i>
                                           <span className="cp-share-button-text">Email</span>
                                       </span>
                                   </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clear"></div>
            </div>
        )
    }
}

export default Shares