import store from  'store/Main'

class Navigation extends React.Component {

    constructor(props){
        super(props)
        this.onViewChange = props.onViewChange
        this.onAdd = props.onAdd
        this.state = {
            show: !store.getState().state.showRemove,
            view: store.getState().state.view,
            className: "navigation"
        }
    }

    componentDidMount() {
        store.subscribe(() => {
                var st = store.getState().state
                this.setState(function(oldState, newState){
                    newState = oldState;
                    if(newState.show != !st.showRemove){
                        newState.show = !st.showRemove
                        newState.className = (!st.showRemove) ? "navigation slideInRight" : "navigation"
                    }
                    newState.view = st.view
                    return newState
                })
            }
        )

    }

    render() {
        var divStyle = {
            display: (this.state.show == true) ? 'block' : 'none',
        }
        return (
            <div id="navigation" className={this.state.className} style={divStyle}>
                <div id="add"  onClick={this.onAdd}></div>
                <div id="switch_view"  onClick={this.onViewChange} className={this.state.view} ></div>
                <div id="save" ></div>
            </div>
        )
    }
}

export default Navigation

