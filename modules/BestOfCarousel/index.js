
class BestOfCarousel extends React.Component {

    constructor(props){
        super(props)
        this.state = {

        }
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <div data-module="bestofcarousel">
                <div className="container">
                    <div className="selector">
                        <label className="prefix">BEST OF &nbsp;</label>
                        <label className="selected"> <span className="value">THE SIX BEST ROOFTOP BARS IN AUCKLAND</span> </label>
                    </div>
                </div>
                <ul className="items">
                    <li className="item ready active" >
                        <a className="grayscale" >
                            <div className="thumbnail">
                                <img src="http://localhost:8080/content/uploads/2016/01/gin-room-519x543.jpg"  />
                            </div>
                            <div className="name">THE SIX BEST HIDDEN BARS IN AUCKLAND</div>
                        </a>
                    </li>
                    <li className="item ready" >
                        <a className="grayscale" >
                            <div className="thumbnail">
                                <img src="http://localhost:8080/content/uploads/2016/01/gin-room-519x543.jpg"  />
                            </div>
                            <div className="name">THE TEN BEST WINTER BARS IN AUCKLAND</div>
                        </a>
                    </li>
                    <li className="item ready" >
                        <a className="grayscale" >
                            <div className="thumbnail">
                                <img src="http://localhost:8080/content/uploads/2016/01/gin-room-519x543.jpg"  />
                            </div>
                            <div className="name">THE SIX BEST ROOFTOP BARS IN AUCKLAND</div>
                        </a>
                    </li>
                    <li className="item ready" >
                        <a className="grayscale" >
                            <div className="thumbnail">
                                <img src="http://localhost:8080/content/uploads/2016/01/gin-room-519x543.jpg"  />
                            </div>
                            <div className="name">THE SIX BEST GIN BARS IN AUCKLAND</div>
                        </a>
                    </li>
                    <li className="item ready" >
                        <a className="grayscale" >
                            <div className="thumbnail">
                                <img src="http://localhost:8080/content/uploads/2016/01/gin-room-519x543.jpg"  />
                            </div>
                            <div className="name">THE SEVEN BEST OYSTER BARS IN AUCKLAND</div>
                        </a>
                    </li>
                </ul>
                <div className="nav prev">
                    <div className="nav-inner">
                        <div className="nav-arrow">
                            <a> <i className="sprite white prev"></i> </a>
                        </div>
                    </div>
                </div>
                <div className="nav next">
                    <div className="nav-inner">
                        <div className="nav-arrow">
                            <a> <i className="sprite white next"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BestOfCarousel