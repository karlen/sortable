
class Desktinations extends React.Component {

    constructor(props){
        super(props)
        this.state = {

        }
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <div data-module="desktinations">
                <h3>Title</h3>
                <h5>Subtitle</h5>
                <ul className="boards">
                    <li className="board">
                        <div className="thumbnail">
                            <a>
                                <div className="canvas cover">
                                    <img  src="http://localhost:8080/content/uploads/2016/06/yochi-hanoi-hannah2-474x308.jpg" />
                                </div>
                                <div className="thumb-mask">
                                    <div className="table">
                                        <div className="cell icon-variant-gold"> <i className="sprite icon36 bar"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="content">
                            <h1 className="title">
                                <a>HANOI HANNAH YO-CHI POP-UP CANTEEN</a></h1>
                            <p>Pho-yo, anyone?</p>
                            <p className="link">
                                <a>
                                    <i className="sprite external gold"></i>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li className="board">
                        <div className="thumbnail">
                            <a>
                                <div className="canvas cover">
                                    <img  src="http://localhost:8080/content/uploads/2016/06/yochi-hanoi-hannah2-474x308.jpg" />
                                </div>
                                <div className="thumb-mask">
                                    <div className="table">
                                        <div className="cell icon-variant-gold"> <i className="sprite icon36 bar"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="content">
                            <h1 className="title">
                                <a>HANOI HANNAH YO-CHI POP-UP CANTEEN</a></h1>
                            <p>Pho-yo, anyone?</p>
                            <p className="link">
                                <a>
                                    <i className="sprite external gold"></i>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li className="board">
                        <div className="thumbnail">
                            <a>
                                <div className="canvas cover">
                                    <img  src="http://localhost:8080/content/uploads/2016/06/yochi-hanoi-hannah2-474x308.jpg" />
                                </div>
                                <div className="thumb-mask">
                                    <div className="table">
                                        <div className="cell icon-variant-gold"> <i className="sprite icon36 bar"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="content">
                            <h1 className="title">
                                <a>HANOI HANNAH YO-CHI POP-UP CANTEEN</a></h1>
                            <p>Pho-yo, anyone?</p>
                            <p className="link">
                                <a>
                                    <i className="sprite external gold"></i>
                                </a>
                            </p>
                        </div>
                    </li>
                </ul>
                <div className="hr"></div>
            </div>
        )
    }
}

export default Desktinations