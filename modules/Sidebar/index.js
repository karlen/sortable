import store from  'store/Main'

class Sidebar extends React.Component {

    constructor(props){
        super(props)
        this.modules = store.getState().state.allModules
        this.onAdd = props.onAdd
        this.state = {
            show: store.getState().state.showSideBar,
            className : (store.getState().state.showSideBar) ? "sidebar slideInLeft" : "sidebar"
        }
    }


    componentDidMount() {
        store.subscribe(() => {
                var st = store.getState().state
                this.setState(function(oldState, newState){
                    newState = oldState;
                    if(newState.show != st.showSideBar){
                        newState.show = st.showSideBar
                        newState.className = (st.showSideBar) ? "sidebar slideInLeft" : "sidebar"
                    }
                    return newState
                })
            }
        )

    }

    render() {
        var divStyle = {
            display: (this.state.show == true) ? 'block' : 'none',
        }
        return (
            <div id="sidebar" className={this.state.className} style={divStyle}>
                <div className="sidebar_inner">
                    {this.modules.map(function(module, idx) {
                        return <button onClick={() => this.onAdd(module.moduleName)} key={idx} className="card-module">
                            <img src={module.img}/>
                            <div className="title">{module.moduleName}</div>
                        </button>;
                    }.bind(this))}
                </div>
            </div>
        )
    }
}

export default Sidebar