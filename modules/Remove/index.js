import store from  'store/Main'

class Remove extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            showRemove: store.getState().state.showRemove,
            hovered: store.getState().state.hoveredRemove,
            className: "remove_button"
        }
    }

    componentDidMount() {
        store.subscribe(() => {
                var st = store.getState().state
                this.setState({
                    showRemove: st.showRemove,
                    hovered: st.hoveredRemove,
                    className: (st.showRemove) ? "remove_button slideInUp" : "remove_button"
                })
            }
        )
    }

    render() {

        var divStyle = {
            display: (this.state.showRemove == true) ? 'block' : 'none',
            border: (this.state.hovered == true) ? '2px solid red' : '2px solid white',
            backgroundImage:(this.state.hovered == false) ? 'url("assets/images/trash.svg")' : 'url("assets/images/trash_active.svg")'
        }

        return (
            <div id="remove_button" className={this.state.className} style={divStyle}></div>
        )
    }
}

export default Remove