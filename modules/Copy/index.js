import store from  'store/Main'

class Copy extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            showCopy: store.getState().state.showRemove,
            hovered: store.getState().state.hoveredCopy,
            className: "copy_button"
        }
    }

    componentDidMount() {
        store.subscribe(() => {
                var st = store.getState().state
                this.setState({
                    showCopy: st.showRemove,
                    hovered: st.hoveredCopy,
                    className: (st.showRemove) ? "copy_button slideInUp" : "copy_button"
                })
            }
        )

    }

    render() {
        var divStyle = {
            display: (this.state.showCopy == true) ? 'block' : 'none',
            border: (this.state.hovered == true) ? '2px solid #91DC5A' : '2px solid white',
            backgroundImage:(this.state.hovered == false) ? 'url("assets/images/copy.svg")' : 'url("assets/images/copy_active.svg")'
        }
        return (
            <div id="copy_button" className={this.state.className} style={divStyle}></div>
        )
    }
}

export default Copy