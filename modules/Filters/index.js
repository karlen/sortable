
class Filters extends React.Component {

    constructor(props){
        super(props)
        this.state = {

        }
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <div data-module="filters">
                <div className="filterbar">
                    <div className="container">
                        <div className="filters" >
                            <div className="filter active">
                                <a href="#" data-filter="all">ALL</a>
                            </div>
                            <div className="filter">
                                <a href="#" data-filter="restaurants">RESTAURANTS</a>
                            </div>
                            <div className="filter">
                                <a href="#" data-filter="bars">BARS</a>
                            </div>
                        </div>
                        <div className="switch_view">
                            <div className="list_view active">
                                <i className="sprite listview"></i>List
                            </div>
                            <div className="map_view">
                                <i className="sprite mapview"></i>Map
                            </div>
                    </div>
                        <div className="clear"></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Filters